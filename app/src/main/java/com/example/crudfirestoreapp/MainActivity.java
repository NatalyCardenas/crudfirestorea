package com.example.crudfirestoreapp;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.provider.DocumentsContract;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;

import java.lang.annotation.Documented;
import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity {
  FirebaseFirestore db;
  TextView txtDisplay;


  @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
         db = FirebaseFirestore.getInstance();
         txtDisplay = (TextView)findViewById(R.id.txtDisplay);

    readSingleContactCustomObject();

    }

  private void addRealTimeUpdate()
  {
    DocumentReference contactListen = db.collection("AddresBook").document("1");
    contactListen.addSnapshotListener(new EventListener<DocumentSnapshot>() {
      @Override
      public void onEvent(DocumentSnapshot documentSnapshot, FirebaseFirestoreException e) {
        if( e != null){
          Log.e("ERROR", e.getMessage());
          return;
        }
        if (documentSnapshot != null && documentSnapshot.exists())
        {
          Toast.makeText(MainActivity.this,"Current data : " +documentSnapshot.getData(), Toast.LENGTH_SHORT).show();
        }
      }
    });
  }

  private void deleteData()
  {
    db.collection("AddresBook").document("1")
      .delete().addOnSuccessListener(new OnSuccessListener<Void>() {
        @Override
        public void onSuccess(Void aVoid) {
          Toast.makeText(MainActivity.this, "Eliminado !!",Toast.LENGTH_SHORT).show();
        }
      })
      .addOnFailureListener(new OnFailureListener() {
        @Override
        public void onFailure(@NonNull Exception e) {
          Toast.makeText(MainActivity.this, "" +e.getMessage(), Toast.LENGTH_SHORT).show();
        }
      });
  }

  private void updateData()
  {
    DocumentReference contact = db.collection("AddresBook").document("1");
    contact.update("name","Adriana Vinueza")
      .addOnSuccessListener(new OnSuccessListener<Void>() {
        @Override
        public void onSuccess(Void aVoid) {
          Toast.makeText(MainActivity.this, "Actualizado...!!", Toast.LENGTH_SHORT).show();
        }
      });

  }

  private void readSingleContactCustomObject()
  {
    DocumentReference contact = db.collection("AddresBook").document("1");
    contact.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
      @Override
      public void onSuccess(DocumentSnapshot documentSnapshot) {
        AddresBook readContact = documentSnapshot.toObject(AddresBook.class);
        StringBuilder data = new StringBuilder("");
        data.append("Name: ").append(readContact.getName());
        data.append("\nEmail: ").append(readContact.getEmail());
        data.append("\nPhone: ").append(readContact.getPhone());
        txtDisplay.setText(data.toString());

      }
    });
  }

  private void readSingleContact()
  {
      DocumentReference contact = db.collection("AddresBook").document("1");
      contact.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
        @Override
        public void onComplete(@NonNull Task<DocumentSnapshot> task) {
          if(task.isSuccessful()){
            DocumentSnapshot doc = task.getResult();
            StringBuilder data = new StringBuilder("");
            data.append("Nombre: ").append(doc.getString("nombre"));
            data.append("\nCorreo: ").append(doc.getString("correo"));
            data.append("\nTelefono: ").append(doc.getString("telefono"));
            txtDisplay.setText(data.toString());
          }
        }
      });
  }

  private void addNewContact()
    {
      Map<String,Object> newContact = new HashMap<>();
      newContact.put("nombre","Nataly");
      newContact.put("correo","ashley.cardenasv@ug.edu.ec");
      newContact.put("telefono","0987541228");
      newContact.put("nombre","Melisa");
      newContact.put("correo","melisa.nunezv@ug.edu.ec");
      newContact.put("telefono","0958966713");


      db.collection("AddresBook").document("2").set(newContact)
        .addOnSuccessListener(new OnSuccessListener<Void>() {
          @Override
          public void onSuccess(Void aVoid) {
            Toast.makeText(MainActivity.this, "Nuevo contacto agregado...", Toast.LENGTH_SHORT).show();
          }
        })
        .addOnFailureListener(new OnFailureListener() {
          @Override
          public void onFailure(@NonNull Exception e) {
            Log.d("ERROR",e.getMessage());
          }
        });
    }
}
