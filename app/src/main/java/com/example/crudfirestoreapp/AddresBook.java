package com.example.crudfirestoreapp;

public class AddresBook {
  private String nombre, correo, telefono;

  public AddresBook(String nombre, String correo, String telefono) {
    this.nombre = nombre;
    this.correo = correo;
    this.telefono = telefono;
  }

  public AddresBook() {
  }

  public String getNombre() {
    return nombre;
  }

  public void setNombre(String nombre) {
    this.nombre = nombre;
  }

  public String getCorreo() {
    return correo;
  }

  public void setCorreo(String correo) {
    this.correo = correo;
  }

  public String getTelefono() {
    return telefono;
  }

  public void setTelefono(String telefono) {
    this.telefono = telefono;
  }
}
